package com.zgw.fireline.base.test;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.rowset.CachedRowSet;

import com.sun.rowset.CachedRowSetImpl;
import com.zgw.fireline.base.DatasetDefine;
import com.zgw.fireline.base.IDataBaseProvide;

public class TestSqlserverProvide implements IDataBaseProvide {

	public TestSqlserverProvide() {

	}

	public CachedRowSet queryForDefine(String defineKey, Object[] param) {

		return null;
	}

	public CachedRowSet queryForSql(String sql, Object[] params) {
		try {
			Connection con = getConnection();
			PreparedStatement p = con.prepareStatement(sql);
			int i = 1;
			for (Object obj : params) {
				p.setObject(i++, obj);
			}
			System.out.println(sql);
			ResultSet r = p.executeQuery();
			CachedRowSetImpl set = new CachedRowSetImpl();
			set.populate(r);
			p.close();
			con.close();

			return set;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Connection getConnection() throws ClassNotFoundException,
			SQLException {
		Class.forName("net.sourceforge.jtds.jdbc.Driver");
		Connection con = DriverManager.getConnection(
				"jdbc:jtds:sqlserver://localhost:1433/medical", "sa", "");
		return con;
	}

	public int getParameterCount(String sql) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int[] execute(String[] command, Map<Integer, Object[]> params) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<DatasetDefine> getAllDataset() {
		try {
			List<DatasetDefine> list = (List<DatasetDefine>) ObjectToXMLUtil
					.objectXmlDecoder(getDefineBeanXml());
			return list;
		} catch (Exception e) {
			throw new RuntimeException("获取数据集定义失败", e);
		}
	}

	public boolean saveDataset(DatasetDefine dataset) {
		try {
			List<DatasetDefine> list = (List<DatasetDefine>) ObjectToXMLUtil
					.objectXmlDecoder(getDefineBeanXml());
			list.add(dataset);
			ObjectToXMLUtil.objectXmlEncoder(list, getDefineBeanXml());
		} catch (Exception e) {
			throw new RuntimeException("保存数据集定义失败", e);
		}
		return true;
	}

	public boolean removeDataset(String defineKey) {
		try {
			List<DatasetDefine> list = (List<DatasetDefine>) ObjectToXMLUtil
					.objectXmlDecoder(getDefineBeanXml());
			DatasetDefine removeDefine = null;
			for (DatasetDefine d : list) {
				if (d.getId().equals(defineKey)) {
					removeDefine = d;
				}
			}
			if (removeDefine != null) {
				list.remove(removeDefine);
				ObjectToXMLUtil.objectXmlEncoder(list, getDefineBeanXml());
			} else {
				throw new RuntimeException("找不到指定的数据集:'" + defineKey + "'");
			}
		} catch (Exception e) {
			throw new RuntimeException("保存数据集定义失败", e);
		}

		return true;
	}

	public DatasetDefine getDataset(String defineKey) {
		try {
			List<DatasetDefine> list;
			list = (List<DatasetDefine>) ObjectToXMLUtil
					.objectXmlDecoder(getDefineBeanXml());
			for (DatasetDefine d : list) {
				if (d.getId().equals(defineKey)) {
					return d;
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("查找数据集定义失败", e);
		}

		return null;
	}

	private String getDefineBeanXml() throws IOException {
		String path = System.getProperty("user.dir") + "/DatasetDefineBean.xml";
		File file = new File(path);
		if (!file.exists()) {
			file.createNewFile();
		}
		return path;
	}

	
}
