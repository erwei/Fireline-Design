package com.zgw.fireline.base.widgets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zgw.fireline.base.IDataBaseProvide;

/**
 * 批处理组件
 * */
public class Batch {
	private final IDataBaseProvide provide;
	private String defineKey; // 系统定义标识
	private List<BatchItem> items = new ArrayList<BatchItem>();// 批处理项

	public Batch(IDataBaseProvide provide) {
		this.provide = provide;
	}

	public Batch(String defineKey, IDataBaseProvide provide) {
		this.defineKey = defineKey;
		this.provide = provide;
	}

	public void setParam(int cIndex, int pIndex, Object param) {
		BatchItem item = items.get(cIndex);
		item.setParam(pIndex, param);
	}

	public int[] execute() {
		String[] command = new String[items.size()];
		Map<Integer, Object[]> params = new HashMap<Integer, Object[]>();
		int index = 0;
		for (BatchItem item : items) {
			command[index] = item.command;
			params.put(index, item.params);
			index++;
		}
		return provide.execute(command, params);
	}

	protected void addItem(BatchItem item) {
		if (!items.contains(item))
			items.add(item);
	}

	public int getItemCount() {
		return items.size();
	}

	public IDataBaseProvide getProvide() {
		return provide;
	}

	/**
	 * 是否系统定义
	 * 
	 * @return
	 * */
	public boolean isSysDefine() {
		return defineKey != null;
	}
}
