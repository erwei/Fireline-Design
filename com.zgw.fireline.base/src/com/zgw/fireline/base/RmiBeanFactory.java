package com.zgw.fireline.base;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import com.zgw.fireline.design.service.IDesignCommonService;

public class RmiBeanFactory {
	private static int port = 9693;
	private static String rmiPath = "rmi://localhost:" + port + "/";

	public static IDesignCommonService getBean(Class class1)
			throws MalformedURLException, RemoteException, NotBoundException {
			return (IDesignCommonService) Naming.lookup(rmiPath
					+ class1.getName());
	}

}
