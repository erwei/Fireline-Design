package com.zgw.fireline.base.controls;

import org.eclipse.swt.widgets.TableColumn;

public class DataTableColumn extends TableColumn {
	private DataLabelProvider labelProvider;
	private final String columnName;
	private String showText;

	public DataTableColumn(DataTable parent, int style, String columnName) {
		super(parent, style);
		this.columnName = columnName;
	}

	public String getColumnName() {
		return columnName;
	}

	public DataLabelProvider getLabelProvider() {
		return labelProvider;
	}

	public void setLabelProvider(DataLabelProvider labelProvider) {
		this.labelProvider = labelProvider;
	}

	public String getShowText() {
		return showText;
	}

	public void setShowText(String showText) {
		this.showText = showText;
	}

	@Override
	protected void checkSubclass() {

	}
}
