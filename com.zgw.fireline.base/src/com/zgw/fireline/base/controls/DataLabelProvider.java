package com.zgw.fireline.base.controls;

import java.text.NumberFormat;

import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;

import com.zgw.fireline.base.dataset.Dataset;

/**
 * 数据标签提供器
 * */
public class DataLabelProvider {

	/**
	 * 获取文本
	 * */
	public String getText(int index, String column, Dataset set) {
		return "";
	}

	public Image getImage(int index, String column, Dataset set) {
		return null;
	}

	public Font getFont(int index, String column, Dataset set) {
		return null;
	}

	public static DataLabelProvider getCurrencyInstance() {
		return new DataLabelProvider() {
			@Override
			public String getText(int index, String column, Dataset set) {
				return NumberFormat.getCurrencyInstance().format(
						set.getValueForIndex(index, column, Double.class));
			}
		};
	}

}
