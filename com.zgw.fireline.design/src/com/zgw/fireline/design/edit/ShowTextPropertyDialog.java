package com.zgw.fireline.design.edit;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.zgw.fireline.design.Model.DatasetInfo;
import com.zgw.fireline.design.common.ExecutionUtils2;

public class ShowTextPropertyDialog extends TitleAreaDialog {
	private Text text;
	private Table table;
	private DatasetInfo dataset;
	public String showText = "";

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public ShowTextPropertyDialog(Shell parentShell, DatasetInfo set) {
		super(parentShell);
		setHelpAvailable(false);
		this.dataset = set;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		getShell().setText("编辑显示文本");
		setMessage("通过拼接数据集属性得到显示文本");
		setTitle("显示文本");
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		Label lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setText("表达示：");

		text = new Text(container, SWT.BORDER | SWT.MULTI);
		GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text.heightHint = 80;
		text.setLayoutData(gd_text);

		Label lblNewLabel_1 = new Label(container, SWT.NONE);
		lblNewLabel_1.setText("数据集属性：");

		table = new Table(container, SWT.BORDER | SWT.FULL_SELECTION);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		TableColumn tblclmnNewColumn = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn.setWidth(200);
		tblclmnNewColumn.setText("属性名");
		table.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				String column = (String) e.item.getData();
				text.insert("${" + column + "}");
			}
		});
		text.insert(showText == null ? "" : showText);
		initializeColumn();
		return area;
	}

	// ======================
	// 初始化column
	// ======================
	private void initializeColumn() {
		// if (!dataset.isInstantiationData()) {
		// try {
		// dataset.updateData();
		// } catch (Exception e) {
		// ExecutionUtils2.openErrorDialog("数据集更新失败", e);
		// }
		// }
		try {
			String[] columns = dataset.getDataColumns();
			if (columns != null) {
				for (String c : columns) {
					TableItem item = new TableItem(table, SWT.NONE);
					item.setText(c);
					item.setData(c);
				}
			}
		} catch (Exception e) {
			ExecutionUtils2.openErrorDialog("未能获得数据集列", e);
		}
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button button = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		button.setText("确定");
		Button button_1 = createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		button_1.setText("关闭");
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(509, 436);
	}

	@Override
	protected void okPressed() {
		showText = text.getText();
		super.okPressed();
	}
}
