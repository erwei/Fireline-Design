package com.zgw.fireline.design.edit;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.internal.core.model.JavaInfoEvaluationHelper;
import org.eclipse.wb.internal.core.utils.reflect.ReflectionUtils;

import com.zgw.fireline.design.Model.DatasetCodeUtil;
import com.zgw.fireline.design.Model.DatasetInfo;
import com.zgw.fireline.design.common.ControlUtil;

public class SelectedKeyDialog extends Dialog {

	protected int result;
	protected Shell shell;
	private Table table;
	private DatasetInfo dataset;

	public String source;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public SelectedKeyDialog(Shell parent, int style, DatasetInfo dataset) {
		super(parent, style);
		setText("SWT Dialog");
		this.dataset = dataset;
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public int open() {
		createContents();
		Display display = getParent().getDisplay();
		ControlUtil.centerShell(display, shell);
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.PRIMARY_MODAL);
		shell.setSize(450, 300);
		shell.setText("主键选中定位");

		Label label = new Label(shell, SWT.NONE);
		label.setBounds(10, 10, 98, 12);
		label.setText("编辑对应主键值：");

		table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setBounds(10, 28, 424, 179);
		table.addListener(SWT.MeasureItem, new Listener() {
			public void handleEvent(Event event) {
				event.height = 25;
			}
		});

		TableColumn tblclmnNewColumn = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn.setResizable(false);
		tblclmnNewColumn.setWidth(100);
		tblclmnNewColumn.setText("列名");

		TableColumn tblclmnNewColumn_1 = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn_1.setResizable(false);
		tblclmnNewColumn_1.setWidth(312);
		tblclmnNewColumn_1.setText("值");

		Button btnNewButton_1 = new Button(shell, SWT.NONE);
		btnNewButton_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				okPressed();
			}
		});
		btnNewButton_1.setBounds(284, 236, 72, 22);
		btnNewButton_1.setText("确定");

		Button btnNewButton = new Button(shell, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				cancel();
			}
		});
		btnNewButton.setBounds(362, 236, 72, 22);
		btnNewButton.setText("取消");

		Label label_1 = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_1.setBounds(0, 223, 454, 2);
		SelectionListener editorListener = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doEditor((TableItem) e.widget.getData());
			}
		};
		for (String k : dataset.getDataKeyColumns()) {
			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(k);
			TableEditor editor = new TableEditor(table);
			Button button = new Button(table, SWT.NONE);
			button.setText("编辑");
			editor.minimumWidth = 45;
			editor.minimumHeight = 24;
			editor.horizontalAlignment = SWT.RIGHT;
			editor.setEditor(button, item, 1);
			button.addSelectionListener(editorListener);
			button.setData(item);
		}
		initializeData();
	}

	private void initializeData() {
		String signature = ReflectionUtils.getMethodSignature("setSelected",
				Object[].class);
		MethodInvocation invo = dataset.getMethodInvocation(signature);
		if (invo!= null) {
			// AstNodeUtils.get
			for (Object arg : invo.arguments()) {
				MethodInvocation m = (MethodInvocation) arg;
				String var = m.getExpression().toString();

				String key = (String) JavaInfoEvaluationHelper
						.getValue((Expression) m.arguments().get(0));
				for (TableItem item : table.getItems()) {
					if (item.getText(0).equalsIgnoreCase(key)) {
						item.setText(1, var + "." + key);
						item.setData(m.toString());
						break;
					}
				}
			}
		}
	}

	protected void cancel() {
		result = SWT.CANCEL;
		shell.close();
	}

	protected void okPressed() {
		result = SWT.OK;
		source = new String();
		for (TableItem item : table.getItems()) {
			if (table.indexOf(item) != 0) {
				source += ",";
			}
			source += item.getData();
		}
		shell.close();
	}

	protected void doEditor(TableItem tableItem) {
		SysSourceSelectDialog dlg = new SysSourceSelectDialog(shell,
				dataset.getRootJava(), Object.class);
		if (dlg.open() == Window.OK) {
			// 设置属性值表达示
			String variab = dlg.source.getVariableSupport().getComponentName();
			String source = variab + "." + dlg.getterMethod + "(\""
					+ dlg.key[0] + "\"";
			if (dlg.getterMethod.equals("getValue")) {
				if (dlg.defaultVar != null) {
					source += ",java.lang.Object.class";
					String defaultCode = DatasetCodeUtil
							.objectToCode(dlg.defaultVar);
					source += "," + defaultCode;
				}
			}
			source += ")";
			tableItem.setText(1, variab + "." + dlg.key[0]);
			tableItem.setData(source);
		}
	}

	protected void doEditor() {

	}
}
