package com.zgw.fireline.design.edit;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.internal.core.utils.jdt.ui.JdtUiUtils;
import org.eclipse.wb.internal.core.utils.state.GlobalState;

/**
 * 数据来源 提供器选择对话框
 * */
public class DataBaseProvideDialog extends TitleAreaDialog {

	private static final String ERROR_MESSAGE = "必须指定一个IDataBaseProvide接口的实现类";
	private IJavaProject project;
	private Combo combClassName;
	public Class<?> dbClass;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public DataBaseProvideDialog(Shell parentShell, IJavaProject project) {
		super(parentShell);
		setHelpAvailable(false);
		this.project = project;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle("数据库指定");
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		GridLayout gl_container = new GridLayout(3, false);
		gl_container.marginWidth = 10;
		gl_container.marginHeight = 10;
		container.setLayout(gl_container);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		CLabel lblNewLabel = new CLabel(container, SWT.NONE);
		lblNewLabel.setToolTipText("com.zgw.fireline.base.IDataBaseProvide");
		lblNewLabel.setText("Class：");

		combClassName = new Combo(container, SWT.NONE);
		combClassName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Button btnNewButton = new Button(container, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doSearchClass();
			}
		});
		btnNewButton.setText(" &查找...");
		setErrorMessage(dbClass == null ? ERROR_MESSAGE : null);
		combClassName.setText(dbClass != null ? dbClass.getName() : "");
		return area;
	}

	// 查找
	protected void doSearchClass() {
		try {
			String className = JdtUiUtils.selectTypeName(getShell(), project);
			if (className == null) {
				setErrorMessage(ERROR_MESSAGE);
				return;
			}
			combClassName.setText(className);
			Class<?> cla0 = GlobalState.getClassLoader().loadClass(className);
			Class<?> cla1 = GlobalState.getClassLoader().loadClass(
					"com.zgw.fireline.base.IDataBaseProvide");
			setErrorMessage(cla1.isAssignableFrom(cla0) ? null : ERROR_MESSAGE);
		} catch (ClassNotFoundException e) {
			setErrorMessage("找不到指定类");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void okPressed() {
		String className = combClassName.getText();
		if (className.trim().equals("")) {
			setErrorMessage(ERROR_MESSAGE);
			return;
		}
		try {
			Class<?> cla0 = GlobalState.getClassLoader().loadClass(className);
			Class<?> cla1 = GlobalState.getClassLoader().loadClass(
					"com.zgw.fireline.base.IDataBaseProvide");
			if (cla1.isAssignableFrom(cla0)) {
				dbClass = cla0;
				super.okPressed();
			} else {
				dbClass = null;
				setErrorMessage(ERROR_MESSAGE);
			}
		} catch (ClassNotFoundException e) {
			setErrorMessage("找不到指定类");
		}
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button button = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		button.setText("确定");
		Button button_1 = createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		button_1.setText("取消");
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}

}
