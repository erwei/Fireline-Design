package com.zgw.fireline.design.Model;

import java.sql.ResultSetMetaData;
import java.util.List;

import org.eclipse.wb.core.model.JavaInfo;
import org.eclipse.wb.internal.core.model.creation.CreationSupport;
import org.eclipse.wb.internal.core.model.description.ComponentDescription;
import org.eclipse.wb.internal.core.model.description.GenericPropertyDescription;
import org.eclipse.wb.internal.core.model.property.JavaProperty;
import org.eclipse.wb.internal.core.model.property.Property;
import org.eclipse.wb.internal.core.model.property.editor.PropertyEditor;
import org.eclipse.wb.internal.core.utils.ast.AstEditor;
import org.eclipse.wb.internal.core.utils.execution.ExecutionUtils;
import org.eclipse.wb.internal.core.utils.execution.RunnableObjectEx;
import org.eclipse.wb.internal.core.utils.reflect.ReflectionUtils;

import com.zgw.fireline.design.edit.SelectedPropertyEditor;
import com.zgw.fireline.design.edit.SourceManagerPropertyEditor;

public class DatasetInfo extends JavaInfo {
	final DatasetProperty dataSourceProperty; // 数据源管理
	private ResultSetMetaData metaData;

	public DatasetInfo(AstEditor editor, ComponentDescription description,
			CreationSupport creationSupport) throws Exception {
		super(editor, description, creationSupport);
		dataSourceProperty = new DatasetProperty(this, "数据源管理",
				new SourceManagerPropertyEditor());
		initDescriptor();
	}

	// 配置 Descriptor 设置属性编辑器
	private void initDescriptor() {
		for (GenericPropertyDescription p : getDescription().getProperties()) {
			if (p.getId().equals("setSelected(java.lang.Object[])")) {
				p.setEditor(new SelectedPropertyEditor());
			}
		}
	}

	@Override
	protected List<Property> getPropertyList() throws Exception {
		List<Property> list = super.getPropertyList();
		list.add(dataSourceProperty);
		return list;
	}

	// ==============================================
	//
	// 以下方法为获取 Dataset 对象信息
	//
	// ==============================================
	public boolean isSysSource() {
		return (Boolean) ReflectionUtils.getFieldObject(getObject(),
				"sysSource");
	}

	public Class<?> getProvideClass() {
		return ExecutionUtils.runObject(new RunnableObjectEx<Class<?>>() {
			public Class<?> runObject() throws Exception {
				Object provide = ReflectionUtils.invokeMethod2(getObject(),
						"getProvide");
				return provide == null ? null : provide.getClass();
			}
		});
	}

	public String getDefineKey() {
		return ExecutionUtils.runObject(new RunnableObjectEx<String>() {
			public String runObject() throws Exception {
				String defineKey = (String) ReflectionUtils.invokeMethod2(
						getObject(), "getDefineKey");
				return defineKey;
			}
		});
	}

	public String getSqlCommand() {
		return ExecutionUtils.runObject(new RunnableObjectEx<String>() {
			public String runObject() throws Exception {
				String sql = (String) ReflectionUtils.invokeMethod2(
						getObject(), "getCommand");
				return sql;
			}
		});
	}

	public Boolean isInstantiationData() {
		return ExecutionUtils.runObject(new RunnableObjectEx<Boolean>() {
			public Boolean runObject() throws Exception {
				return (Boolean) ReflectionUtils.invokeMethod2(getObject(),
						"isInstantiation");
			}
		});
	}

	public void updateData() throws Exception {
		ReflectionUtils.invokeMethod2(getObject(), "update");
	}

	public String[] getDataColumns() throws Exception {
		// 初始化 元数据
		String sql = (String) ReflectionUtils.invokeMethod2(getObject(),
				"getCommand");
		metaData = IDataBaseProvideHelp.getSetMetaData(getProvideClass(), sql);
		String[] columns = new String[metaData.getColumnCount()];
		for (int i = 0; i < columns.length; i++) {
			columns[i] = metaData.getColumnLabel(i + 1);
		}
		return columns;
		// return ExecutionUtils.runObject(new RunnableObjectEx<String[]>() {
		// public String[] runObject() throws Exception {
		// return (String[]) ReflectionUtils.invokeMethod2(getObject(),
		// "getColumns");
		// }
		// });
	}

	public Class<?> getDataType(final String column) throws Exception {
		// 初始化 元数据
		if (metaData == null) {
			String sql = (String) ReflectionUtils.invokeMethod2(getObject(),
					"getCommand");
			metaData = IDataBaseProvideHelp.getSetMetaData(getProvideClass(),
					sql);
		}
		for (int i = 1; i <= metaData.getColumnCount(); i++) {
			if (metaData.getColumnName(i).equalsIgnoreCase(column))
				return Class.forName(metaData.getColumnClassName(i));
		}
		// return ExecutionUtils.runObject(new RunnableObjectEx<Class<?>>() {
		// public Class<?> runObject() throws Exception {
		// return (Class<?>) ReflectionUtils.invokeMethod2(getObject(),
		// "getType", String.class, column);
		// }
		// });
		return null;
	}

	public String[] getDataKeyColumns() {
		return ExecutionUtils.runObject(new RunnableObjectEx<String[]>() {
			public String[] runObject() throws Exception {
				return (String[]) ReflectionUtils.invokeMethod2(getObject(),
						"getKeyColumns");
			}
		});
	}

	/**
	 * {@link Dataset} 数据集属性
	 * */
	private static class DatasetProperty extends JavaProperty {

		public DatasetProperty(JavaInfo javaInfo, String title,
				PropertyEditor propertyEditor) {
			super(javaInfo, title, propertyEditor);
		}

		@Override
		public boolean isModified() throws Exception {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public Object getValue() throws Exception {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void setValue(Object value) throws Exception {
			// TODO Auto-generated method stub
		}
	}

}
