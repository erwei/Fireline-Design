package com.zgw.fireline.design.Model;

import org.eclipse.wb.core.model.JavaInfo;
import org.eclipse.wb.internal.core.model.creation.CreationSupport;
import org.eclipse.wb.internal.core.model.description.ComponentDescription;
import org.eclipse.wb.internal.core.model.presentation.DefaultJavaInfoPresentation;
import org.eclipse.wb.internal.core.model.presentation.IObjectPresentation;
import org.eclipse.wb.internal.core.utils.ast.AstEditor;
import org.eclipse.wb.internal.core.utils.reflect.ReflectionUtils;

import com.zgw.fireline.design.edit.Param;

public class BatchItemInfo extends JavaInfo {
	protected String name;
	protected int type;
	protected String command;// 执行命令 脚本
	protected Param[] params = new Param[0];
	private IObjectPresentation m_presentation;

	public BatchItemInfo(AstEditor editor, ComponentDescription description,
			CreationSupport creationSupport) throws Exception {
		super(editor, description, creationSupport);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Param[] getParams() {
		return params;
	}

	public void setParams(Param[] params) {
		this.params = params;
	}

	@Override
	public IObjectPresentation getPresentation() {
		if (m_presentation == null) {
			m_presentation = new DefaultJavaInfoPresentation(this) {
				public String getText() throws Exception {
					return ""+getPropertyByTitle("name").getValue();
				};
			};
		}
		return m_presentation;
	}

	/**
	 * 刷新属性
	 * */
	public void refreshProperty() {
		if (getVariableSupport() == null)
			return;
		try {
			name = (String) getPropertyByTitle("name").getValue();
			type = (Integer) getPropertyByTitle("type").getValue();
			command = (String) getPropertyByTitle("command").getValue();
		} catch (Exception e) {
			ReflectionUtils.propagate(e); // 重新抛出异常
		}

	}
}
